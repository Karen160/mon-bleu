# Mon Bleu

Lorsque j'étais en freelance en tant que développeuse web, j'ai réalisé le site Mon Bleu en Wordpress avec les plugins : WPBackery Page Builder, WooCommerce et Slider Revolution pour l'agence Purée Maison.
Consultez le site via ce lien : https://www.monbleu.fr/
